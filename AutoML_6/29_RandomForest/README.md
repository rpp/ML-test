# Summary of 29_RandomForest

## Random Forest
- **criterion**: mse
- **max_features**: 0.9
- **min_samples_split**: 40
- **max_depth**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

24.4 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.062046  |
| MSE      | 0.0296034 |
| RMSE     | 0.172057  |
| R2       | 0.126482  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)