# Summary of 37_RandomForest_SelectedFeatures

## Random Forest
- **criterion**: mse
- **max_features**: 1.0
- **min_samples_split**: 30
- **max_depth**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

18.6 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0617236 |
| MSE      | 0.0295003 |
| RMSE     | 0.171756  |
| R2       | 0.129527  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)