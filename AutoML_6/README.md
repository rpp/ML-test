# AutoML Leaderboard

| Best model   | name                                               | model_type     | metric_type   |   metric_value |   train_time | Link                                                                         |
|:-------------|:---------------------------------------------------|:---------------|:--------------|---------------:|-------------:|:-----------------------------------------------------------------------------|
|              | 1_Linear                                           | Linear         | rmse          |      0.18349   |         7.25 | [Results link](1_Linear/README.md)                                           |
|              | 2_Default_RandomForest                             | Random Forest  | rmse          |      0.17751   |        17.59 | [Results link](2_Default_RandomForest/README.md)                             |
|              | 3_Default_Xgboost                                  | Xgboost        | rmse          |      0.14097   |      1261.03 | [Results link](3_Default_Xgboost/README.md)                                  |
|              | 4_Default_LightGBM                                 | LightGBM       | rmse          |      0.138722  |       742.05 | [Results link](4_Default_LightGBM/README.md)                                 |
|              | 5_Default_CatBoost                                 | CatBoost       | rmse          |      0.125514  |        66.25 | [Results link](5_Default_CatBoost/README.md)                                 |
|              | 6_Default_NeuralNetwork                            | Neural Network | rmse          |      0.083756  |       636.87 | [Results link](6_Default_NeuralNetwork/README.md)                            |
|              | 11_LightGBM                                        | LightGBM       | rmse          |      0.141848  |        55.33 | [Results link](11_LightGBM/README.md)                                        |
|              | 12_LightGBM                                        | LightGBM       | rmse          |      0.134771  |        32.71 | [Results link](12_LightGBM/README.md)                                        |
|              | 13_LightGBM                                        | LightGBM       | rmse          |      0.137555  |        20.46 | [Results link](13_LightGBM/README.md)                                        |
|              | 14_LightGBM                                        | LightGBM       | rmse          |      0.133632  |        51.17 | [Results link](14_LightGBM/README.md)                                        |
|              | 15_CatBoost                                        | CatBoost       | rmse          |      0.153138  |        91.4  | [Results link](15_CatBoost/README.md)                                        |
|              | 16_CatBoost                                        | CatBoost       | rmse          |      0.132063  |        33.58 | [Results link](16_CatBoost/README.md)                                        |
|              | 17_CatBoost                                        | CatBoost       | rmse          |      0.127567  |        34.89 | [Results link](17_CatBoost/README.md)                                        |
|              | 18_CatBoost                                        | CatBoost       | rmse          |      0.144063  |        58.17 | [Results link](18_CatBoost/README.md)                                        |
|              | 19_RandomForest                                    | Random Forest  | rmse          |      0.177608  |        17.71 | [Results link](19_RandomForest/README.md)                                    |
|              | 20_RandomForest                                    | Random Forest  | rmse          |      0.180234  |        19.16 | [Results link](20_RandomForest/README.md)                                    |
|              | 21_RandomForest                                    | Random Forest  | rmse          |      0.172084  |        23.09 | [Results link](21_RandomForest/README.md)                                    |
|              | 5_Default_CatBoost_GoldenFeatures                  | CatBoost       | rmse          |      0.0675518 |        28.14 | [Results link](5_Default_CatBoost_GoldenFeatures/README.md)                  |
|              | 14_LightGBM_GoldenFeatures                         | LightGBM       | rmse          |      0.066537  |        15.14 | [Results link](14_LightGBM_GoldenFeatures/README.md)                         |
|              | 14_LightGBM_GoldenFeatures_RandomFeature           | LightGBM       | rmse          |      0.0666619 |        15.71 | [Results link](14_LightGBM_GoldenFeatures_RandomFeature/README.md)           |
|              | 5_Default_CatBoost_GoldenFeatures_SelectedFeatures | CatBoost       | rmse          |      0.0672265 |        24.57 | [Results link](5_Default_CatBoost_GoldenFeatures_SelectedFeatures/README.md) |
|              | 14_LightGBM_GoldenFeatures_SelectedFeatures        | LightGBM       | rmse          |      0.0659679 |        13.37 | [Results link](14_LightGBM_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 21_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.171774  |        19.14 | [Results link](21_RandomForest_SelectedFeatures/README.md)                   |
|              | 22_CatBoost_GoldenFeatures_SelectedFeatures        | CatBoost       | rmse          |      0.068311  |        30.44 | [Results link](22_CatBoost_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 23_CatBoost_GoldenFeatures                         | CatBoost       | rmse          |      0.069749  |        25.26 | [Results link](23_CatBoost_GoldenFeatures/README.md)                         |
|              | 24_LightGBM_GoldenFeatures_SelectedFeatures        | LightGBM       | rmse          |      0.0659679 |        13.17 | [Results link](24_LightGBM_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 25_LightGBM_GoldenFeatures                         | LightGBM       | rmse          |      0.066537  |        15.09 | [Results link](25_LightGBM_GoldenFeatures/README.md)                         |
|              | 28_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.171396  |        16.92 | [Results link](28_RandomForest_SelectedFeatures/README.md)                   |
|              | 29_RandomForest                                    | Random Forest  | rmse          |      0.172057  |        24.9  | [Results link](29_RandomForest/README.md)                                    |
|              | 30_CatBoost_GoldenFeatures_SelectedFeatures        | CatBoost       | rmse          |      0.0672265 |        24.96 | [Results link](30_CatBoost_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 31_CatBoost_GoldenFeatures                         | CatBoost       | rmse          |      0.0675518 |        26.5  | [Results link](31_CatBoost_GoldenFeatures/README.md)                         |
|              | 32_LightGBM_GoldenFeatures_SelectedFeatures        | LightGBM       | rmse          |      0.0660332 |        13.47 | [Results link](32_LightGBM_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 33_LightGBM_GoldenFeatures_SelectedFeatures        | LightGBM       | rmse          |      0.0660332 |        13.53 | [Results link](33_LightGBM_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 35_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.171383  |        18.12 | [Results link](35_RandomForest_SelectedFeatures/README.md)                   |
|              | 36_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.171526  |        16.85 | [Results link](36_RandomForest_SelectedFeatures/README.md)                   |
|              | 37_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.171756  |        19.08 | [Results link](37_RandomForest_SelectedFeatures/README.md)                   |
|              | 38_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.171932  |        19.1  | [Results link](38_RandomForest_SelectedFeatures/README.md)                   |
| **the best** | Ensemble                                           | Ensemble       | rmse          |      0.0642041 |         3.21 | [Results link](Ensemble/README.md)                                           |

### AutoML Performance
![AutoML Performance](ldb_performance.png)

### AutoML Performance Boxplot
![AutoML Performance Boxplot](ldb_performance_boxplot.png)