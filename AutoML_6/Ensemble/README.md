# Summary of Ensemble

## Ensemble structure
| Model                                              |   Weight |
|:---------------------------------------------------|---------:|
| 14_LightGBM_GoldenFeatures_SelectedFeatures        |        7 |
| 22_CatBoost_GoldenFeatures_SelectedFeatures        |        3 |
| 32_LightGBM_GoldenFeatures_SelectedFeatures        |       11 |
| 5_Default_CatBoost_GoldenFeatures                  |        2 |
| 5_Default_CatBoost_GoldenFeatures_SelectedFeatures |        7 |
| 6_Default_NeuralNetwork                            |        4 |

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0129631  |
| MSE      | 0.00412217 |
| RMSE     | 0.0642041  |
| R2       | 0.878366   |



## Learning curves
![Learning curves](learning_curves.png)