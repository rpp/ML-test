# Summary of 18_CatBoost

## CatBoost
- **learning_rate**: 0.1
- **depth**: 3
- **rsm**: 0.9
- **subsample**: 0.7
- **min_data_in_leaf**: 5
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

57.7 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.075507  |
| MSE      | 0.0207541 |
| RMSE     | 0.144063  |
| R2       | 0.387604  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)