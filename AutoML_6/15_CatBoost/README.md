# Summary of 15_CatBoost

## CatBoost
- **learning_rate**: 0.1
- **depth**: 2
- **rsm**: 0.8
- **subsample**: 0.7
- **min_data_in_leaf**: 5
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

90.9 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.080977  |
| MSE      | 0.0234512 |
| RMSE     | 0.153138  |
| R2       | 0.30802   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)