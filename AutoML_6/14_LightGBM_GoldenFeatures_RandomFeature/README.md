# Summary of 14_LightGBM_GoldenFeatures_RandomFeature

## LightGBM
- **objective**: regression
- **metric**: l2
- **num_leaves**: 31
- **learning_rate**: 0.05
- **feature_fraction**: 0.9
- **bagging_fraction**: 1.0
- **min_data_in_leaf**: 20
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

15.2 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0126186  |
| MSE      | 0.00444381 |
| RMSE     | 0.0666619  |
| R2       | 0.868875   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)