# Summary of 3_Default_Xgboost

## Extreme Gradient Boosting (Xgboost)
- **objective**: reg:squarederror
- **eval_metric**: rmse
- **eta**: 0.1
- **max_depth**: 4
- **min_child_weight**: 1
- **subsample**: 1.0
- **colsample_bytree**: 1.0
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

1260.5 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0714375 |
| MSE      | 0.0198726 |
| RMSE     | 0.14097   |
| R2       | 0.413613  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)