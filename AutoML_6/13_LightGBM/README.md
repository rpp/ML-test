# Summary of 13_LightGBM

## LightGBM
- **objective**: regression
- **metric**: l2
- **num_leaves**: 15
- **learning_rate**: 0.15
- **feature_fraction**: 0.8
- **bagging_fraction**: 0.9
- **min_data_in_leaf**: 20
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

20.0 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0621154 |
| MSE      | 0.0189213 |
| RMSE     | 0.137555  |
| R2       | 0.441682  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)