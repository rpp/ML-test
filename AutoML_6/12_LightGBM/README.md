# Summary of 12_LightGBM

## LightGBM
- **objective**: regression
- **metric**: l2
- **num_leaves**: 15
- **learning_rate**: 0.1
- **feature_fraction**: 0.8
- **bagging_fraction**: 1.0
- **min_data_in_leaf**: 50
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

32.2 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0638915 |
| MSE      | 0.0181632 |
| RMSE     | 0.134771  |
| R2       | 0.464052  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)