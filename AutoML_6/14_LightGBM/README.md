# Summary of 14_LightGBM

## LightGBM
- **objective**: regression
- **metric**: l2
- **num_leaves**: 31
- **learning_rate**: 0.05
- **feature_fraction**: 0.9
- **bagging_fraction**: 1.0
- **min_data_in_leaf**: 20
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

50.7 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0564891 |
| MSE      | 0.0178576 |
| RMSE     | 0.133632  |
| R2       | 0.473072  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)