# Summary of 16_CatBoost

## CatBoost
- **learning_rate**: 0.2
- **depth**: 5
- **rsm**: 1.0
- **subsample**: 0.8
- **min_data_in_leaf**: 1
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

33.1 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0689995 |
| MSE      | 0.0174407 |
| RMSE     | 0.132063  |
| R2       | 0.485372  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)