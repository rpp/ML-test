# Summary of 5_Default_CatBoost

## CatBoost
- **learning_rate**: 0.1
- **depth**: 6
- **rsm**: 0.9
- **subsample**: 1.0
- **min_data_in_leaf**: 15
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

65.8 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.062225  |
| MSE      | 0.0157538 |
| RMSE     | 0.125514  |
| R2       | 0.535147  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)