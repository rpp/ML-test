# Summary of 23_CatBoost_GoldenFeatures

## CatBoost
- **learning_rate**: 0.1
- **depth**: 5
- **rsm**: 0.9
- **subsample**: 1.0
- **min_data_in_leaf**: 15
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

24.8 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0177163  |
| MSE      | 0.00486492 |
| RMSE     | 0.069749   |
| R2       | 0.856449   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)