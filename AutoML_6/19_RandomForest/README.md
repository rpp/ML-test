# Summary of 19_RandomForest

## Random Forest
- **criterion**: mse
- **max_features**: 0.5
- **min_samples_split**: 20
- **max_depth**: 6
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

17.3 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0642606 |
| MSE      | 0.0315446 |
| RMSE     | 0.177608  |
| R2       | 0.0692044 |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)