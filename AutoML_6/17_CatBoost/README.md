# Summary of 17_CatBoost

## CatBoost
- **learning_rate**: 0.2
- **depth**: 6
- **rsm**: 0.9
- **subsample**: 1.0
- **min_data_in_leaf**: 20
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

34.4 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0654884 |
| MSE      | 0.0162733 |
| RMSE     | 0.127567  |
| R2       | 0.51982   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)