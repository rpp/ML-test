# Summary of 36_RandomForest_SelectedFeatures

## Random Forest
- **criterion**: mse
- **max_features**: 0.9
- **min_samples_split**: 50
- **max_depth**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

16.4 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0615885 |
| MSE      | 0.0294212 |
| RMSE     | 0.171526  |
| R2       | 0.131861  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)