# AutoML Leaderboard

| Best model   | name                    | model_type     | metric_type   |   metric_value |   train_time | Link                                              |
|:-------------|:------------------------|:---------------|:--------------|---------------:|-------------:|:--------------------------------------------------|
|              | 1_Linear                | Linear         | rmse          |      0.182688  |         7.41 | [Results link](1_Linear/README.md)                |
|              | 2_Default_RandomForest  | Random Forest  | rmse          |      0.176298  |       123.96 | [Results link](2_Default_RandomForest/README.md)  |
|              | 3_Default_Xgboost       | Xgboost        | rmse          |      0.110364  |      1001.77 | [Results link](3_Default_Xgboost/README.md)       |
|              | 4_Default_LightGBM      | LightGBM       | rmse          |      0.0953088 |      1350.73 | [Results link](4_Default_LightGBM/README.md)      |
|              | 5_Default_CatBoost      | CatBoost       | rmse          |      0.0951652 |       740.78 | [Results link](5_Default_CatBoost/README.md)      |
|              | 6_Default_NeuralNetwork | Neural Network | rmse          |      0.0915909 |       429.71 | [Results link](6_Default_NeuralNetwork/README.md) |
| **the best** | Ensemble                | Ensemble       | rmse          |      0.0843868 |         0.15 | [Results link](Ensemble/README.md)                |

### AutoML Performance
![AutoML Performance](ldb_performance.png)

### AutoML Performance Boxplot
![AutoML Performance Boxplot](ldb_performance_boxplot.png)