# Summary of 2_Default_RandomForest

## Random Forest
- **criterion**: mse
- **max_features**: 0.6
- **min_samples_split**: 30
- **max_depth**: 6
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

123.0 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0633206 |
| MSE      | 0.0310809 |
| RMSE     | 0.176298  |
| R2       | 0.0754198 |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)