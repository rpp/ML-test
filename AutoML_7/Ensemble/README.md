# Summary of Ensemble

## Ensemble structure
| Model                   |   Weight |
|:------------------------|---------:|
| 4_Default_LightGBM      |        1 |
| 5_Default_CatBoost      |        1 |
| 6_Default_NeuralNetwork |        2 |

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0299202  |
| MSE      | 0.00712113 |
| RMSE     | 0.0843868  |
| R2       | 0.788164   |



## Learning curves
![Learning curves](learning_curves.png)