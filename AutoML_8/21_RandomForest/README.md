# Summary of 21_RandomForest

## Random Forest
- **criterion**: mse
- **max_features**: 1.0
- **min_samples_split**: 40
- **max_depth**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

173.2 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0596863 |
| MSE      | 0.0289096 |
| RMSE     | 0.170028  |
| R2       | 0.140011  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)