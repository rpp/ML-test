# Summary of 8_Xgboost

## Extreme Gradient Boosting (Xgboost)
- **objective**: reg:squarederror
- **eval_metric**: rmse
- **eta**: 0.05
- **max_depth**: 4
- **min_child_weight**: 6
- **subsample**: 1.0
- **colsample_bytree**: 0.4
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

5822.6 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0625763 |
| MSE      | 0.0131096 |
| RMSE     | 0.114497  |
| R2       | 0.610022  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)