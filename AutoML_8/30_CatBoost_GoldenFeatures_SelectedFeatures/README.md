# Summary of 30_CatBoost_GoldenFeatures_SelectedFeatures

## CatBoost
- **learning_rate**: 0.1
- **depth**: 6
- **rsm**: 1
- **subsample**: 1.0
- **min_data_in_leaf**: 15
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

154.3 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0126852 |
| MSE      | 0.0029769 |
| RMSE     | 0.0545609 |
| R2       | 0.911445  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)