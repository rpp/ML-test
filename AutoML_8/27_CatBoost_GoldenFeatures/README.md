# Summary of 27_CatBoost_GoldenFeatures

## CatBoost
- **learning_rate**: 0.1
- **depth**: 6
- **rsm**: 0.8
- **subsample**: 1.0
- **min_data_in_leaf**: 15
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

191.0 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0127467  |
| MSE      | 0.00299557 |
| RMSE     | 0.0547318  |
| R2       | 0.910889   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)