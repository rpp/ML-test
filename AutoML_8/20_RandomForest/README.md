# Summary of 20_RandomForest

## Random Forest
- **criterion**: mse
- **max_features**: 0.7
- **min_samples_split**: 50
- **max_depth**: 4
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

79.1 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0646099 |
| MSE      | 0.032024  |
| RMSE     | 0.178953  |
| R2       | 0.0473642 |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)