# Summary of 28_CatBoost_GoldenFeatures

## CatBoost
- **learning_rate**: 0.1
- **depth**: 6
- **rsm**: 1
- **subsample**: 1.0
- **min_data_in_leaf**: 15
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

185.2 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0128199  |
| MSE      | 0.00298337 |
| RMSE     | 0.0546202  |
| R2       | 0.911252   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)