# Summary of 25_NeuralNetwork_SelectedFeatures

## Neural Network
- **dense_layers**: 2
- **dense_1_size**: 64
- **dense_2_size**: 32
- **dropout**: 0.0
- **learning_rate**: 0.1
- **momentum**: 0.85
- **decay**: 0.001
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

10007.3 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.00854493 |
| MSE      | 0.00321187 |
| RMSE     | 0.0566734  |
| R2       | 0.904455   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)