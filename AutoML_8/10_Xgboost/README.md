# Summary of 10_Xgboost

## Extreme Gradient Boosting (Xgboost)
- **objective**: reg:squarederror
- **eval_metric**: rmse
- **eta**: 0.025
- **max_depth**: 3
- **min_child_weight**: 9
- **subsample**: 0.4
- **colsample_bytree**: 1.0
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

2979.2 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0693607 |
| MSE      | 0.0169056 |
| RMSE     | 0.130022  |
| R2       | 0.4971    |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)