# Summary of Ensemble

## Ensemble structure
| Model                                       |   Weight |
|:--------------------------------------------|---------:|
| 14_LightGBM_GoldenFeatures_SelectedFeatures |       10 |
| 25_NeuralNetwork                            |        5 |
| 25_NeuralNetwork_SelectedFeatures           |        3 |
| 29_CatBoost_GoldenFeatures_SelectedFeatures |        3 |
| 33_NeuralNetwork                            |        7 |
| 34_NeuralNetwork_SelectedFeatures           |        3 |
| 43_LightGBM_GoldenFeatures_SelectedFeatures |        9 |
| 44_LightGBM_GoldenFeatures                  |        5 |

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.00780642 |
| MSE      | 0.00244164 |
| RMSE     | 0.049413   |
| R2       | 0.927367   |



## Learning curves
![Learning curves](learning_curves.png)