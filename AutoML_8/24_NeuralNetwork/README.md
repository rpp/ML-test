# Summary of 24_NeuralNetwork

## Neural Network
- **dense_layers**: 2
- **dense_1_size**: 64
- **dense_2_size**: 16
- **dropout**: 0.0
- **learning_rate**: 0.1
- **momentum**: 0.9
- **decay**: 0.01
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

14491.4 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0176111  |
| MSE      | 0.00697392 |
| RMSE     | 0.08351    |
| R2       | 0.792543   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)