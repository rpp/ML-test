# Summary of 35_RandomForest_SelectedFeatures

## Random Forest
- **criterion**: mse
- **max_features**: 0.6
- **min_samples_split**: 40
- **max_depth**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

81.0 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0595421 |
| MSE      | 0.0283492 |
| RMSE     | 0.168372  |
| R2       | 0.156682  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)