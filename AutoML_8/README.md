# AutoML Leaderboard

| Best model   | name                                               | model_type     | metric_type   |   metric_value |   train_time | Link                                                                         |
|:-------------|:---------------------------------------------------|:---------------|:--------------|---------------:|-------------:|:-----------------------------------------------------------------------------|
|              | 1_Linear                                           | Linear         | rmse          |      0.182688  |        10.94 | [Results link](1_Linear/README.md)                                           |
|              | 2_Default_RandomForest                             | Random Forest  | rmse          |      0.176298  |       123.56 | [Results link](2_Default_RandomForest/README.md)                             |
|              | 3_Default_Xgboost                                  | Xgboost        | rmse          |      0.110364  |      7806.38 | [Results link](3_Default_Xgboost/README.md)                                  |
|              | 4_Default_LightGBM                                 | LightGBM       | rmse          |      0.0953088 |      3679.01 | [Results link](4_Default_LightGBM/README.md)                                 |
|              | 5_Default_CatBoost                                 | CatBoost       | rmse          |      0.0951652 |       738.62 | [Results link](5_Default_CatBoost/README.md)                                 |
|              | 6_Default_NeuralNetwork                            | Neural Network | rmse          |      0.0605738 |     12010.3  | [Results link](6_Default_NeuralNetwork/README.md)                            |
|              | 7_Xgboost                                          | Xgboost        | rmse          |      0.158709  |      1807.59 | [Results link](7_Xgboost/README.md)                                          |
|              | 8_Xgboost                                          | Xgboost        | rmse          |      0.114497  |      5823.61 | [Results link](8_Xgboost/README.md)                                          |
|              | 9_Xgboost                                          | Xgboost        | rmse          |      0.122724  |      2006.14 | [Results link](9_Xgboost/README.md)                                          |
|              | 10_Xgboost                                         | Xgboost        | rmse          |      0.130022  |      2980.27 | [Results link](10_Xgboost/README.md)                                         |
|              | 11_LightGBM                                        | LightGBM       | rmse          |      0.105287  |      8965.32 | [Results link](11_LightGBM/README.md)                                        |
|              | 12_LightGBM                                        | LightGBM       | rmse          |      0.0950386 |      1312.72 | [Results link](12_LightGBM/README.md)                                        |
|              | 13_LightGBM                                        | LightGBM       | rmse          |      0.0967456 |       525.92 | [Results link](13_LightGBM/README.md)                                        |
|              | 14_LightGBM                                        | LightGBM       | rmse          |      0.0919319 |      4350.84 | [Results link](14_LightGBM/README.md)                                        |
|              | 15_CatBoost                                        | CatBoost       | rmse          |      0.140992  |       569.55 | [Results link](15_CatBoost/README.md)                                        |
|              | 16_CatBoost                                        | CatBoost       | rmse          |      0.101888  |       369.41 | [Results link](16_CatBoost/README.md)                                        |
|              | 17_CatBoost                                        | CatBoost       | rmse          |      0.0972406 |       373.77 | [Results link](17_CatBoost/README.md)                                        |
|              | 18_CatBoost                                        | CatBoost       | rmse          |      0.122348  |       687.86 | [Results link](18_CatBoost/README.md)                                        |
|              | 19_RandomForest                                    | Random Forest  | rmse          |      0.176166  |        92.09 | [Results link](19_RandomForest/README.md)                                    |
|              | 20_RandomForest                                    | Random Forest  | rmse          |      0.178953  |        79.98 | [Results link](20_RandomForest/README.md)                                    |
|              | 21_RandomForest                                    | Random Forest  | rmse          |      0.170028  |       174.11 | [Results link](21_RandomForest/README.md)                                    |
|              | 22_RandomForest                                    | Random Forest  | rmse          |      0.169083  |       138.66 | [Results link](22_RandomForest/README.md)                                    |
|              | 23_NeuralNetwork                                   | Neural Network | rmse          |      0.0948786 |      8930.73 | [Results link](23_NeuralNetwork/README.md)                                   |
|              | 24_NeuralNetwork                                   | Neural Network | rmse          |      0.08351   |     14492.3  | [Results link](24_NeuralNetwork/README.md)                                   |
|              | 25_NeuralNetwork                                   | Neural Network | rmse          |      0.0566292 |     11657    | [Results link](25_NeuralNetwork/README.md)                                   |
|              | 26_NeuralNetwork                                   | Neural Network | rmse          |      0.0608596 |     13082.8  | [Results link](26_NeuralNetwork/README.md)                                   |
|              | 5_Default_CatBoost_GoldenFeatures                  | CatBoost       | rmse          |      0.0545591 |       211.64 | [Results link](5_Default_CatBoost_GoldenFeatures/README.md)                  |
|              | 14_LightGBM_GoldenFeatures                         | LightGBM       | rmse          |      0.0523123 |       261.13 | [Results link](14_LightGBM_GoldenFeatures/README.md)                         |
|              | 3_Default_Xgboost_GoldenFeatures                   | Xgboost        | rmse          |      0.0635525 |       292.07 | [Results link](3_Default_Xgboost_GoldenFeatures/README.md)                   |
|              | 14_LightGBM_GoldenFeatures_RandomFeature           | LightGBM       | rmse          |      0.0523322 |       228.61 | [Results link](14_LightGBM_GoldenFeatures_RandomFeature/README.md)           |
|              | 5_Default_CatBoost_GoldenFeatures_SelectedFeatures | CatBoost       | rmse          |      0.0546331 |       185.06 | [Results link](5_Default_CatBoost_GoldenFeatures_SelectedFeatures/README.md) |
|              | 14_LightGBM_GoldenFeatures_SelectedFeatures        | LightGBM       | rmse          |      0.0519517 |       290.8  | [Results link](14_LightGBM_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 25_NeuralNetwork_SelectedFeatures                  | Neural Network | rmse          |      0.0566734 |     10008.2  | [Results link](25_NeuralNetwork_SelectedFeatures/README.md)                  |
|              | 22_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.168934  |        79.63 | [Results link](22_RandomForest_SelectedFeatures/README.md)                   |
|              | 3_Default_Xgboost_GoldenFeatures_SelectedFeatures  | Xgboost        | rmse          |      0.062798  |       329.94 | [Results link](3_Default_Xgboost_GoldenFeatures_SelectedFeatures/README.md)  |
|              | 27_CatBoost_GoldenFeatures                         | CatBoost       | rmse          |      0.0547318 |       191.99 | [Results link](27_CatBoost_GoldenFeatures/README.md)                         |
|              | 28_CatBoost_GoldenFeatures                         | CatBoost       | rmse          |      0.0546202 |       186.15 | [Results link](28_CatBoost_GoldenFeatures/README.md)                         |
|              | 29_CatBoost_GoldenFeatures_SelectedFeatures        | CatBoost       | rmse          |      0.0541167 |       217.27 | [Results link](29_CatBoost_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 30_CatBoost_GoldenFeatures_SelectedFeatures        | CatBoost       | rmse          |      0.0545609 |       155.3  | [Results link](30_CatBoost_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 31_LightGBM_GoldenFeatures_SelectedFeatures        | LightGBM       | rmse          |      0.0529851 |       372.17 | [Results link](31_LightGBM_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 32_LightGBM_GoldenFeatures                         | LightGBM       | rmse          |      0.0533224 |       384.99 | [Results link](32_LightGBM_GoldenFeatures/README.md)                         |
|              | 33_NeuralNetwork                                   | Neural Network | rmse          |      0.055973  |     13179.7  | [Results link](33_NeuralNetwork/README.md)                                   |
|              | 34_NeuralNetwork_SelectedFeatures                  | Neural Network | rmse          |      0.0569385 |     11131.7  | [Results link](34_NeuralNetwork_SelectedFeatures/README.md)                  |
|              | 35_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.168372  |        81.97 | [Results link](35_RandomForest_SelectedFeatures/README.md)                   |
|              | 36_RandomForest                                    | Random Forest  | rmse          |      0.168902  |       138.19 | [Results link](36_RandomForest/README.md)                                    |
|              | 37_Xgboost_GoldenFeatures_SelectedFeatures         | Xgboost        | rmse          |      0.0635872 |       331.87 | [Results link](37_Xgboost_GoldenFeatures_SelectedFeatures/README.md)         |
|              | 38_Xgboost_GoldenFeatures_SelectedFeatures         | Xgboost        | rmse          |      0.0627064 |       236.86 | [Results link](38_Xgboost_GoldenFeatures_SelectedFeatures/README.md)         |
|              | 39_Xgboost_GoldenFeatures                          | Xgboost        | rmse          |      0.063308  |       504.63 | [Results link](39_Xgboost_GoldenFeatures/README.md)                          |
|              | 40_Xgboost_GoldenFeatures                          | Xgboost        | rmse          |      0.0629382 |       257.63 | [Results link](40_Xgboost_GoldenFeatures/README.md)                          |
|              | 41_CatBoost_GoldenFeatures_SelectedFeatures        | CatBoost       | rmse          |      0.0541167 |       220.35 | [Results link](41_CatBoost_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 42_CatBoost_GoldenFeatures                         | CatBoost       | rmse          |      0.0545591 |       204.13 | [Results link](42_CatBoost_GoldenFeatures/README.md)                         |
|              | 43_LightGBM_GoldenFeatures_SelectedFeatures        | LightGBM       | rmse          |      0.0521604 |       200.78 | [Results link](43_LightGBM_GoldenFeatures_SelectedFeatures/README.md)        |
|              | 44_LightGBM_GoldenFeatures                         | LightGBM       | rmse          |      0.0523224 |       187.51 | [Results link](44_LightGBM_GoldenFeatures/README.md)                         |
|              | 45_NeuralNetwork                                   | Neural Network | rmse          |      0.055798  |     10397.4  | [Results link](45_NeuralNetwork/README.md)                                   |
|              | 46_NeuralNetwork                                   | Neural Network | rmse          |      0.0557968 |     13398.5  | [Results link](46_NeuralNetwork/README.md)                                   |
|              | 47_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.172183  |        71.62 | [Results link](47_RandomForest_SelectedFeatures/README.md)                   |
|              | 48_RandomForest_SelectedFeatures                   | Random Forest  | rmse          |      0.163706  |        90.69 | [Results link](48_RandomForest_SelectedFeatures/README.md)                   |
|              | 49_RandomForest                                    | Random Forest  | rmse          |      0.172408  |       132.8  | [Results link](49_RandomForest/README.md)                                    |
|              | 50_RandomForest                                    | Random Forest  | rmse          |      0.1654    |       147.6  | [Results link](50_RandomForest/README.md)                                    |
|              | 51_Xgboost_GoldenFeatures_SelectedFeatures         | Xgboost        | rmse          |      0.0678564 |       324.27 | [Results link](51_Xgboost_GoldenFeatures_SelectedFeatures/README.md)         |
|              | 52_Xgboost_GoldenFeatures_SelectedFeatures         | Xgboost        | rmse          |      0.0682553 |       377.15 | [Results link](52_Xgboost_GoldenFeatures_SelectedFeatures/README.md)         |
| **the best** | Ensemble                                           | Ensemble       | rmse          |      0.049413  |        13.91 | [Results link](Ensemble/README.md)                                           |

### AutoML Performance
![AutoML Performance](ldb_performance.png)

### AutoML Performance Boxplot
![AutoML Performance Boxplot](ldb_performance_boxplot.png)