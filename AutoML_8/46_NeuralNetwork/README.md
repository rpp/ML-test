# Summary of 46_NeuralNetwork

## Neural Network
- **dense_layers**: 2
- **dense_1_size**: 64
- **dense_2_size**: 32
- **dropout**: 0.1
- **learning_rate**: 0.1
- **momentum**: 0.85
- **decay**: 0.001
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

13397.6 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.00841606 |
| MSE      | 0.00311328 |
| RMSE     | 0.0557968  |
| R2       | 0.907388   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)