# Summary of 44_LightGBM_GoldenFeatures

## LightGBM
- **objective**: regression
- **metric**: l2
- **num_leaves**: 31
- **learning_rate**: 0.075
- **feature_fraction**: 0.9
- **bagging_fraction**: 1.0
- **min_data_in_leaf**: 20
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

186.5 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.00881335 |
| MSE      | 0.00273763 |
| RMSE     | 0.0523224  |
| R2       | 0.918562   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)