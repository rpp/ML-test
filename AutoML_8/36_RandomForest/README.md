# Summary of 36_RandomForest

## Random Forest
- **criterion**: mse
- **max_features**: 0.6
- **min_samples_split**: 40
- **max_depth**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

137.3 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0598644 |
| MSE      | 0.0285279 |
| RMSE     | 0.168902  |
| R2       | 0.151366  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)