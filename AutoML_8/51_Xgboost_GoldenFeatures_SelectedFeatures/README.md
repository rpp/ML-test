# Summary of 51_Xgboost_GoldenFeatures_SelectedFeatures

## Extreme Gradient Boosting (Xgboost)
- **objective**: reg:squarederror
- **eval_metric**: rmse
- **eta**: 0.15
- **max_depth**: 3
- **min_child_weight**: 1
- **subsample**: 1.0
- **colsample_bytree**: 1.0
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

323.3 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.022987   |
| MSE      | 0.00460449 |
| RMSE     | 0.0678564  |
| R2       | 0.863028   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)