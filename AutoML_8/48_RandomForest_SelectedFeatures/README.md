# Summary of 48_RandomForest_SelectedFeatures

## Random Forest
- **criterion**: mse
- **max_features**: 0.6
- **min_samples_split**: 40
- **max_depth**: 12
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

89.8 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.05748   |
| MSE      | 0.0267997 |
| RMSE     | 0.163706  |
| R2       | 0.202774  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)