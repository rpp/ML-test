# Summary of 34_NeuralNetwork_SelectedFeatures

## Neural Network
- **dense_layers**: 2
- **dense_1_size**: 64
- **dense_2_size**: 16
- **dropout**: 0.0
- **learning_rate**: 0.1
- **momentum**: 0.85
- **decay**: 0.001
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

11130.8 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0081901 |
| MSE      | 0.003242  |
| RMSE     | 0.0569385 |
| R2       | 0.903559  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)