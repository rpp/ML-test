# Summary of 9_Xgboost

## Extreme Gradient Boosting (Xgboost)
- **objective**: reg:squarederror
- **eval_metric**: rmse
- **eta**: 0.15
- **max_depth**: 3
- **min_child_weight**: 5
- **subsample**: 1.0
- **colsample_bytree**: 0.3
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

2005.1 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.070187  |
| MSE      | 0.0150612 |
| RMSE     | 0.122724  |
| R2       | 0.551967  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)