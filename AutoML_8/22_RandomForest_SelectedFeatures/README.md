# Summary of 22_RandomForest_SelectedFeatures

## Random Forest
- **criterion**: mse
- **max_features**: 0.6
- **min_samples_split**: 50
- **max_depth**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

78.7 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0596569 |
| MSE      | 0.0285387 |
| RMSE     | 0.168934  |
| R2       | 0.151044  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)