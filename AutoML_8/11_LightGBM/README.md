# Summary of 11_LightGBM

## LightGBM
- **objective**: regression
- **metric**: l2
- **num_leaves**: 7
- **learning_rate**: 0.05
- **feature_fraction**: 0.9
- **bagging_fraction**: 0.9
- **min_data_in_leaf**: 10
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

8964.3 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0513528 |
| MSE      | 0.0110854 |
| RMSE     | 0.105287  |
| R2       | 0.670237  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)