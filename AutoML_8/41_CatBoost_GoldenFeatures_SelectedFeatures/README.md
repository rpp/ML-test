# Summary of 41_CatBoost_GoldenFeatures_SelectedFeatures

## CatBoost
- **learning_rate**: 0.1
- **depth**: 6
- **rsm**: 0.8
- **subsample**: 0.9
- **min_data_in_leaf**: 15
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

219.4 seconds

### Metric details:
| Metric   |      Score |
|:---------|-----------:|
| MAE      | 0.0126755  |
| MSE      | 0.00292862 |
| RMSE     | 0.0541167  |
| R2       | 0.912881   |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)