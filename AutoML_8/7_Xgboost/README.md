# Summary of 7_Xgboost

## Extreme Gradient Boosting (Xgboost)
- **objective**: reg:squarederror
- **eval_metric**: rmse
- **eta**: 0.01
- **max_depth**: 2
- **min_child_weight**: 2
- **subsample**: 0.9
- **colsample_bytree**: 0.4
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

1806.6 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0774776 |
| MSE      | 0.0251885 |
| RMSE     | 0.158709  |
| R2       | 0.250705  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)