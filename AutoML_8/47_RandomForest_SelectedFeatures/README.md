# Summary of 47_RandomForest_SelectedFeatures

## Random Forest
- **criterion**: mse
- **max_features**: 0.6
- **min_samples_split**: 40
- **max_depth**: 8
- **explain_level**: 1

## Validation
 - **validation_type**: kfold
 - **k_folds**: 5
 - **shuffle**: True

## Optimized metric
rmse

## Training time

70.7 seconds

### Metric details:
| Metric   |     Score |
|:---------|----------:|
| MAE      | 0.0611829 |
| MSE      | 0.0296469 |
| RMSE     | 0.172183  |
| R2       | 0.118077  |



## Learning curves
![Learning curves](learning_curves.png)

## Permutation-based Importance
![Permutation-based Importance](permutation_importance.png)